FROM cloudron/base:0.10.0
MAINTAINER Aleksandr Bogdanov <syn+rssbridgecloudron@syn.im>

# this can be removed in next base image update
RUN apt-get update && \
    apt-get install -y php-zip crudini php-soap php-imap zip && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code
WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log

RUN ln -s /run/apache2/app.conf /etc/apache2/sites-enabled/app.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite authnz_ldap

COPY . /app/code
RUN rm -r /app/code/cache && ln -s /app/data/cache /app/code/cache

CMD [ "/app/code/start.sh" ]