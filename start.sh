#!/bin/bash
[ -e "/app/data/cache" ] || mkdir /app/data/cache
chown www-data:www-data /app/data/cache

cp /app/code/apache2-app.conf /run/apache2/app.conf
# Configure LDAP
if [ ! -z "${LDAP_URL}" ]; then
    sed -e "s@AuthLDAPURL .*@AuthLDAPURL ${LDAP_URL}/${LDAP_USERS_BASE_DN}?username??(objectclass=user)@" \
        -e "s@AuthLDAPBindDN .*@AuthLDAPBindDN ${LDAP_BIND_DN}@" \
        -e "s@AuthLDAPBindPassword .*@AuthLDAPBindPassword ${LDAP_BIND_PASSWORD}@" \
        -e "s@Require .*@Require valid-user@" \
        -i /run/apache2/app.conf
fi

# Run apache
source /etc/apache2/envvars
/usr/sbin/apache2 -DFOREGROUND